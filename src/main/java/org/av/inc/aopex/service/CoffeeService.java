package org.av.inc.aopex.service;

import org.springframework.stereotype.Service;

@Service("coffeeService")
public class CoffeeService implements ICoffeeService {
    @Override
    public String plant(String Id) {
        return "Tree planted: " + Id; 
    }

    @Override
    public long count() {
        return 100000000;
    }

    @Override
    public String grow() {
        return "Growing good!";
    }

    @Override
    public String fetchBeans(String Id) {
        return "Beans fetched from Tree: " + Id;
    }
}
