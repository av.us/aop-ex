package org.av.inc.aopex.service;

public interface ICoffeeService {
    public String plant(String Id);
    public long count();
    public String grow();
    public String fetchBeans(String Id);
}
