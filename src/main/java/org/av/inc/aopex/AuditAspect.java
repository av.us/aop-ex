package org.av.inc.aopex;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class AuditAspect {
    private static Logger logger = LoggerFactory.getLogger(AuditAspect.class);

    @Pointcut("execution(* org.av.inc.aopex.service.*.*(..))")
    public void pointcutMethods(){} //see point-cut syntax
    
    //aspect
    @Before("pointcutMethods()")
    public void beforeMethod() {
    	logger.info("---------<before method>-------------");
    }

   //aspect
    @Around("pointcutMethods()")
    public Object aroundMethod(ProceedingJoinPoint joinpoint) {
        try {
       	 	logger.info(String.format("%s : %s", "---begin <around method>--->", joinpoint.getSignature()));
       	 	//
            Object result = joinpoint.proceed();
       	 	//
            logger.info(String.format("%s : %s", "---end <around method>--->", joinpoint.getSignature()));
            return result;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
   
    //aspect  
    @After("pointcutMethods()")
    public void afterMethod() {
     	logger.info("--------<after method>--------------");
    }    
    
}