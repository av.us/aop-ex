package org.av.inc.aopex.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;

import org.av.inc.aopex.AuditAspect;

@Configuration
public class AppConfig {
	
	@Bean
	public AuditAspect auditAspect() {
		return new AuditAspect();
	}	
}
