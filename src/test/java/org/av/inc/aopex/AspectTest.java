package org.av.inc.aopex;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.av.inc.Application;
import org.av.inc.aopex.service.ICoffeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;;

@RunWith(SpringRunner.class)
//@SpringBootTest(classes = Application.class) 
@ContextConfiguration(classes = Application.class) 
public class AspectTest {
	
    @Autowired 
    ICoffeeService coffeeService;
    
    @Test
    public void testCoffeeService() {
        String tree = this.coffeeService.plant("New 1");
        assertThat(tree, is("Tree planted: New 1"));
        
        long treeCount = this.coffeeService.count();
        assertThat(treeCount, is(100000000L));
   
        
        String growit = this.coffeeService.grow();
        assertThat(growit, is("Growing good!"));
        
        String fetchStatus=this.coffeeService.fetchBeans("Tree2");
        assertThat(fetchStatus, is("Beans fetched from Tree: Tree2"));
    }

}
