# Spring AOP - Example Dummy Coffee Service with @Apsect
## `mvn test` or `click run junit test on AspectTest`
<pre>
INFO org.av.inc.aopex.AuditAspect - ---begin <around method>---> : String org.av.inc.aopex.service.CoffeeService.plant(String)
INFO org.av.inc.aopex.AuditAspect - ---------<before method>-------------
INFO org.av.inc.aopex.AuditAspect - ---end <around method>---> : String org.av.inc.aopex.service.CoffeeService.plant(String)
INFO org.av.inc.aopex.AuditAspect - --------<after method>--------------
INFO org.av.inc.aopex.AuditAspect - ---begin <around method>---> : long org.av.inc.aopex.service.CoffeeService.count()
INFO org.av.inc.aopex.AuditAspect - ---------<before method>-------------
INFO org.av.inc.aopex.AuditAspect - ---end <around method>---> : long org.av.inc.aopex.service.CoffeeService.count()
INFO org.av.inc.aopex.AuditAspect - --------<after method>--------------
INFO org.av.inc.aopex.AuditAspect - ---begin <around method>---> : String org.av.inc.aopex.service.CoffeeService.grow()
INFO org.av.inc.aopex.AuditAspect - ---------<before method>-------------
INFO org.av.inc.aopex.AuditAspect - ---end <around method>---> : String org.av.inc.aopex.service.CoffeeService.grow()
INFO org.av.inc.aopex.AuditAspect - --------<after method>--------------
INFO org.av.inc.aopex.AuditAspect - ---begin <around method>---> : String org.av.inc.aopex.service.CoffeeService.fetchBeans(String)
INFO org.av.inc.aopex.AuditAspect - ---------<before method>-------------
INFO org.av.inc.aopex.AuditAspect - ---end <around method>---> : String org.av.inc.aopex.service.CoffeeService.fetchBeans(String)
INFO org.av.inc.aopex.AuditAspect - --------<after method>--------------
</pre>
